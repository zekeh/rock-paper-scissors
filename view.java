import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class view extends JPanel{
	static String rock = "rock";
	static String paper = "paper";
	static String scissors = "scissors";
	private JRadioButton rockButton;
	private JRadioButton paperButton;
	private JRadioButton scissorsButton;
	private JTextField computerChoice;
	private JTextArea winOrLose;
	private model m;

	private int compChoice;
	private int userChoice;
	private controller c;
	public view(){
		super();
		this.m = m;
		this.c = c;
		rockButton = new JRadioButton(rock);
		rockButton.setActionCommand(rock);

		paperButton = new JRadioButton(paper);
		paperButton.setActionCommand(paper);

		scissorsButton = new JRadioButton(scissors);
		scissorsButton.setActionCommand(scissors);

		ButtonGroup group = new ButtonGroup();
		group.add(rockButton);
		group.add(paperButton);
		group.add(scissorsButton);

		rockButton.addActionListener(new ButtonHandler());
		paperButton.addActionListener(new ButtonHandler());
		scissorsButton.addActionListener(new ButtonHandler());

		JPanel radioPanel = new JPanel();
		radioPanel.add(rockButton);
		radioPanel.add(paperButton);
		radioPanel.add(scissorsButton);
		add(radioPanel);

		computerChoice = new JTextField("", 20);
		computerChoice.setEditable(false);

		add(computerChoice);

		winOrLose = new JTextArea(5, 20);
		winOrLose.setEditable(false);
		winOrLose.insert("", 0);
		add(winOrLose);

		
	}
	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			compChoice = m.computer();
			switch(m.computer()){
				case 1:
					computerChoice.setText("computer chose rock.");
					break;
				case 2:
					computerChoice.setText("computer chose paper.");
					break;
				case 3:
					computerChoice.setText("computer chose scissors.");
					break;
		}
			if(e.getSource()==paperButton){
				userChoice = 2;
		}
			if(e.getSource()==rockButton){
				userChoice = 1;
			}
			if(e.getSource()==scissorsButton){
				userChoice = 3;
			}
			switch(m.winner(compChoice, userChoice)){
				case 1:
					winOrLose.setText("you won!");
					break;
				case 0:
					winOrLose.setText("you lost!");
					break;
				case -1:
					winOrLose.setText("you tied!");
					break;
			}
        }
    }

}