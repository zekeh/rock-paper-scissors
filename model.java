import java.util.Random;
import java.util.Scanner;

public class model{

 /**
 * testing
 * @param
 */
/*public static void main(String[] args){
		int numOfLoops = 0;
		int won = 0;
	while (true){
		int choice = choose();
		int compChoice = computer();
		rockPaperScissors(choice);
		won += winner(compChoice, choice);
		numOfLoops += 1;
		System.out.println("number of wins: ");
		System.out.println(won);
		System.out.println("total times played: ");
		System.out.println(numOfLoops);
		System.out.println("\n");
	}
}*/

	/**
	 * displays rock, paper, or scissors
	 * @param the number which represents rock, paper, or scissors
	 */
	public static void rockPaperScissors(int num){
		if (num == 1){
			System.out.println("rock\n");
		}
		if (num == 2){
			System.out.println("paper\n");
		}
		if(num == 3){
			System.out.println("scissors\n");
		}
	}
	/**
	 * determines what computer chose
	 * @return
	 */
	public static int computer(){
		Random random = new Random();
		int num = random.nextInt(3) + 1;
		return num;
	}
	/**
	 * lets user choose between rock, paper, and scissors
	 * @return choice
	 */
	public static int choose(){
		Scanner input = new Scanner(System.in);
		System.out.print("choose 1(rock), 2(paper), 3(scissors):\n");
		int choice = input.nextInt();
		return choice;
	}
	/**
	 * determines who won
	 * @param computers choice
	 * @param users choice
	 */
	public static int winner(int compNum, int userNum){
		if(compNum == userNum){
			System.out.println("tie!\n");
		}
		else{
		switch(compNum){
			case 1:
				if(userNum == 2){
					System.out.println("you win!\n");
					return 1;
				}
				if(userNum == 3){
					System.out.println("you lose.\n");
					return 0;
				//break;
			}
			case 2:
				if(userNum == 3){
					System.out.println("you win!\n");
					return 1;
				}
				if(userNum == 1){
					System.out.println("you lose.\n");
					return 0;
				//break;
			}
			case 3:
				if(userNum == 1){
					System.out.println("you win!\n");
					return 1;
				}
				if(userNum == 2){
					System.out.println("you lose.\n");
					return 0;
				}
				//break;
		}
	}
	return -1; //tie
}

}